<?php

namespace Drupal\api_consumer_helper;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\State\State;
use Psr\Log\LoggerInterface;

/**
 * Helper class for loggers.
 */
class LoggerHelper {

  /**
   * Drupal api_consumer_helper service: 'api_consumer_helper.logger'.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $loggerApiCall;

  /**
   * Drupal core service: 'entity_type.manager'.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Drupal core service: 'current_user'.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * Returns TRUE if Drupal's set to debug mode.
   *
   * @var bool
   */
  protected bool $isDebugMode;

  /**
   * Construct for LoggerHelper class.
   *
   * @param \Psr\Log\LoggerInterface $loggerApiCall
   *   Drupal api_consumer_helper service: 'api_consumer_helper.logger'.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Drupal core service: 'entity_type.manager'.
   * @param \Drupal\Core\State\State $state
   *   Drupal core service: 'state'.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   Drupal core service: 'current_user'.
   */
  public function __construct(
    LoggerInterface $loggerApiCall,
    EntityTypeManager $entityTypeManager,
    State $state,
    AccountProxy $currentUser,
  ) {
    $this->loggerApiCall = $loggerApiCall;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->isDebugMode = ($state->get('is_debug_mode', FALSE) === TRUE);
  }

  /**
   * Log an API call.
   *
   * @param float $call_time
   *   Call duration, in seconds.
   * @param string $method
   *   HTTP verb, case-insensitive.
   * @param mixed $status_code
   *   Response's status code.
   * @param string $endpoint
   *   Call endpoint.
   * @param mixed[]|null $request_params
   *   Call endpoint's parameters.
   * @param mixed|null $request_contents
   *   Call endpoint's response contents.
   */
  public function logApiCall(
    float $call_time,
    string $method,
    mixed $status_code,
    string $endpoint,
    array $request_params = NULL,
    mixed $request_contents = NULL
  ): void {
    $call_time_str = match ((string) $call_time) {
      '-1' => 'AlreadyInRequest',
      default => number_format($call_time, 2, ',', '.') . ' s.',
    };
    $method = strtoupper($method);
    if (!$this->isDebugMode) {
      $endpoint_split = explode('?', $endpoint);
      $endpoint = $endpoint_split[0];
    }
    $triggered_by = $this->getApiCallTriggeredBy();
    $log_message = "$call_time_str / $method / $status_code / $triggered_by / $endpoint";

    if ($this->isDebugMode && !empty($request_params)) {
      $log_message .= ' with params ' . json_encode($request_params);
    }
    if ($this->isDebugMode && !empty($request_contents)) {
      if (is_scalar($request_contents)) {
        $log_message .= " responded $request_contents";
      }
      elseif (is_array($request_contents)) {
        $log_message .= ' responded ' . json_encode($request_contents);
      }
    }

    $log_level = $this->getApiCallLogLevel($status_code, $call_time);
    $this->loggerApiCall->{$log_level}($log_message);
  }

  /**
   * Log an API error.
   *
   * @param string $api_name
   *   API name.
   * @param string $error_description
   *   Error description.
   */
  public function logApiError(string $api_name, string $error_description): void {
    $triggered_by = $this->getApiCallTriggeredBy();
    $this->loggerApiCall->error("$api_name / $error_description / $triggered_by");
  }

  /**
   * Returns a string for the user who triggered the api call.
   *
   * @return string
   *   The user who triggered the api call
   */
  private function getApiCallTriggeredBy(): string {
    if ($this->currentUser->isAnonymous()) {
      return 'by anonymous';
    }
    else {
      return 'by Drupal user ' . $this->currentUser->getDisplayName();
    }
  }

  /**
   * Returns the appropriate log level for the API call.
   *
   * @param mixed $status_code
   *   Request's status code.
   * @param float $call_time
   *   Call duration, in seconds.
   *
   * @return string
   *   The log level.
   */
  private function getApiCallLogLevel(mixed $status_code, float $call_time): string {
    if (is_numeric($status_code)) {
      if ($status_code >= 300) {
        $log_level = 'error';
      }
      elseif ($status_code >= 200 || $status_code === 0) {
        $log_level = 'debug';
      }
    }

    if ($call_time >= 10) {
      $log_level = 'warning';
    }

    return $log_level ?? 'warning';
  }

}
