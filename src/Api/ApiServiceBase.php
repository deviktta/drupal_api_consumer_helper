<?php

namespace Drupal\api_consumer_helper\Api;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\State;
use Drupal\api_consumer_helper\LoggerHelper;
use Drupal\user\UserData;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Base class to consume APIs.
 */
abstract class ApiServiceBase {

  /**
   * Drupal core service: 'settings'.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $settings;

  /**
   * Drupal core service: 'config_factory'.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * Drupal core service: 'state'.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * Drupal core service: 'entity_type.manager'.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Drupal core service: 'http_client_factory'.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected ClientFactory $httpClientFactory;

  /**
   * Drupal core service: 'current_user'.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * Drupal user service: 'user.data'.
   *
   * @var \Drupal\user\UserData
   */
  protected UserData $userData;

  /**
   * Drupal api_consumer_helper service: 'api_consumer_helper.logger'.
   *
   * @var \Drupal\api_consumer_helper\LoggerHelper
   */
  protected LoggerHelper $loggerHelper;

  /**
   * Constructor for class ApiServiceBase.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   Drupal core service: 'settings'.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Drupal core service: 'config.factory'.
   * @param \Drupal\Core\State\State $state
   *   Drupal core service: 'state'.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Drupal core service: 'entity_type.manager'.
   * @param \Drupal\Core\Http\ClientFactory $clientFactory
   *   Drupal core service: 'http_client_factory'.
   * @param \Drupal\Core\Session\AccountProxy $accountProxy
   *   Drupal core service: 'current_user'.
   * @param \Drupal\user\UserData $userData
   *   Drupal user service: 'user.data'.
   * @param \Drupal\api_consumer_helper\LoggerHelper $loggerHelper
   *   Drupal api_consumer_helper service: 'api_consumer_helper.logger'.
   */
  public function __construct(
    Settings $settings,
    ConfigFactory $configFactory,
    State $state,
    EntityTypeManager $entityTypeManager,
    ClientFactory $clientFactory,
    AccountProxy $accountProxy,
    UserData $userData,
    LoggerHelper $loggerHelper
  ) {
    $this->settings = $settings;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->entityTypeManager = $entityTypeManager;
    $this->httpClientFactory = $clientFactory;
    $this->currentUser = $accountProxy;
    $this->userData = $userData;
    $this->loggerHelper = $loggerHelper;
  }

  /**
   * Returns an HTTP Client.
   *
   * @return \GuzzleHttp\Client
   *   An HTTP Client.
   */
  protected function getClient(): Client {
    return $this->httpClientFactory->fromOptions();
  }

  /**
   * Returns the request's response.
   *
   * @param string $method
   *   Request method.
   * @param string $uri
   *   Request URI.
   * @param mixed[] $options
   *   Request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Request's response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request(string $method, string $uri = '', array $options = []): ResponseInterface {
    $client = $this->getClient();

    try {
      $call_start_time = microtime(TRUE);
      $response = $client->request($method, $uri, $options);
      $call_time = microtime(TRUE) - $call_start_time;
      $status_code = $response->getStatusCode();
      $contents = json_decode($response->getBody()->getContents(), TRUE);
      $response->getBody()->rewind();
      $this->loggerHelper->logApiCall($call_time, $method, $status_code, $uri, $options, $contents);

      return $response;
    }
    catch (GuzzleException $e) {
      $call_time = microtime(TRUE) - $call_start_time;
      $this->loggerHelper->logApiCall($call_time, $method, $e->getCode(), $uri);
      throw $e;
    }
  }

}
