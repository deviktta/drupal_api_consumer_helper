<?php

namespace Drupal\api_consumer_helper\Api;

/**
 * This exception interface must be implemented in all API exceptions.
 */
class ApiException extends \Exception implements ApiExceptionInterface {}
