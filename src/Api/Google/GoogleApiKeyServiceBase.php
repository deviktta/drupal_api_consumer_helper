<?php

namespace Drupal\api_consumer_helper\Api\Google;

use Drupal\api_consumer_helper\Api\ApiException;
use Drupal\api_consumer_helper\Api\ApiServiceBase;
use Psr\Http\Message\ResponseInterface;

/**
 * Base class to consumer Google APIs based on API keys.
 */
abstract class GoogleApiKeyServiceBase extends ApiServiceBase {

  /**
   * Returns the settings key to store the Google API key.
   *
   * @return string
   *   The setting name.
   */
  abstract protected function getApiKeySettingName(): string;

  /**
   * Returns the Google API key.
   *
   * @return string
   *   The Google API key.
   *
   * @throws \Drupal\api_consumer_helper\Api\ApiExceptionInterface
   */
  private function getApiKey(): string {
    $settings_name = $this->getApiKeySettingName();
    $key = $this->settings::get($settings_name);

    if (!is_string($key)) {
      throw new ApiException("Google API key not set in \$settings['$settings_name'].");
    }

    return $key;
  }

  /**
   * Returns the request's response.
   *
   * @param string $method
   *   Request method.
   * @param string $uri
   *   Request URI.
   * @param mixed[] $options
   *   Request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Request's response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\api_consumer_helper\Api\ApiExceptionInterface
   */
  protected function request(string $method, string $uri = '', array $options = []): ResponseInterface {
    $options['query'] = ($options['query'] ?? []) + ['key' => $this->getApiKey()];
    return parent::request($method, $uri, $options);
  }

}
