<?php

namespace Drupal\api_consumer_helper\Api\Google\PlacesAPI;

use Drupal\api_consumer_helper\Api\ApiException;

/**
 * Exception class for Google Places API.
 */
class PlacesApiException extends ApiException {

}
