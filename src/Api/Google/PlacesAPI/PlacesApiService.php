<?php

namespace Drupal\api_consumer_helper\Api\Google\PlacesAPI;

use Drupal\api_consumer_helper\Api\ApiExceptionInterface;
use Drupal\api_consumer_helper\Api\Google\GoogleApiKeyServiceBase;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Service class to consume Google Places API.
 */
class PlacesApiService extends GoogleApiKeyServiceBase {
  public const ENDPOINT_PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json";
  public const ENDPOINT_PLACE_SEARCH_TEXTSEARCH = "https://maps.googleapis.com/maps/api/place/textsearch/json";

  /**
   * {@inheritDoc}
   */
  public function getApiKeySettingName(): string {
    return 'api_consumer_helper__google_places_api_key';
  }

  /**
   * Returns the search results.
   *
   * @param string $search_text
   *   The text to search for.
   * @param string|null $type
   *   The type of place (restaurant, hospital...)
   *
   * @return mixed[]
   *   The search results.
   *
   * @throws \Drupal\api_consumer_helper\Api\ApiException
   */
  public function searchPlacesByText(string $search_text, ?string $type = NULL): array {
    $options = [
      'query' => [
        'query' => $search_text,
        'type' => $type,
      ],
    ];

    try {
      $response = $this->request('get', self::ENDPOINT_PLACE_SEARCH_TEXTSEARCH, $options);
      return json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (ApiExceptionInterface | GuzzleException $e) {
      $this->loggerHelper->logApiError(
        'Google Places API',
        $e->getMessage()
      );
      return [];
    }
  }

  /**
   * Returns the data for a given Google Place id.
   *
   * @param string $place_id
   *   The Google Place id.
   *
   * @return mixed[]
   *   The Google Place data.
   *
   * @throws \Drupal\api_consumer_helper\Api\ApiExceptionInterface
   */
  public function getPlaceInfo(string $place_id): array {
    $options = [
      'query' => [
        'place_id' => $place_id,
        'fields' => 'name,place_id,geometry,rating,formatted_phone_number,formatted_address',
      ],
    ];

    try {
      $response = $this->request('get', self::ENDPOINT_PLACE_DETAILS, $options);
      return json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (ApiExceptionInterface | GuzzleException $e) {
      $this->loggerHelper->logApiError(
        'Google Places API',
        $e->getMessage()
      );
      return [];
    }
  }

}
